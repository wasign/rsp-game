import { Option } from './types';
export const MAX_ACTIVE_BETS = 2;
export const START_SUM = 5000;
export const BET_SIZE = 500;

export const OPTIONS: Option[] = ['Rock', 'Paper', 'Scissors'];
export const OPTIONS_ORDERED_FOR_COMPARE: Option[] = ['Rock', 'Paper', 'Scissors'];

export const EMOJIS: Record<Option, string> = {
  Paper: '📜',
  Rock: '🪨',
  Scissors: '✂'
}
