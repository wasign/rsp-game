export type Option = 'Rock' | 'Paper' | 'Scissors';

export enum Multiplicator {
  singleOption = 14,
  doubleOption = 2
}

export interface GameResult {
  result: 'win' | 'lose';
  payoff: number;
  winningPlayerOption?: Option;
}