import React, { useEffect } from 'react';
import { Option } from './types';
import { OPTIONS, EMOJIS } from './constants'
import { chooseRandom } from './helpers'
import './App.css';

export const SelectionProcess: React.FC = () => {
  const [selection, setSelection] = React.useState<Option>("Paper");
  useEffect(() => {
    const intervalId = setInterval(
      () => {
        setSelection(chooseRandom(OPTIONS))
      }, 100
    );
    return () => {
      clearInterval(intervalId)
    }
  }, [])
  return <div className='content'>
    {`Computer choosing: ${EMOJIS[selection]}`}
  </div>
}