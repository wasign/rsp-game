import React from 'react';
import cx from 'classnames';
import './BettingOption.css';

interface BettingOptionProps {
  increaseBet: () => void;
  decreaseBet: () => void;
  increaseDisabled?: boolean;
  decreaseDisabled?: boolean;
  disabled?: boolean;
}

export const BettingOption: React.FC<BettingOptionProps> = ({ disabled, increaseBet, decreaseBet, children, decreaseDisabled, increaseDisabled }) => {
  return <div className={cx('betting-option--container')}>
    <button
      onClick={decreaseBet}
      disabled={disabled || decreaseDisabled}
      className={cx('bet-change', 'bet-change__decrease')}
    >
      –
    </button>
    <span>{children}</span>
    <button
      className={cx('bet-change', 'bet-change__increase')}
      disabled={disabled || increaseDisabled}
      onClick={increaseBet}
    >
      +
  </button>


  </div>
}