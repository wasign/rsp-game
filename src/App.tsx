import React from 'react';
import { Option } from './types';
import { BET_SIZE, MAX_ACTIVE_BETS, START_SUM, OPTIONS, EMOJIS } from './constants';
import { chooseRandom, getBetsCount, getGameResult, getSubtitle } from './helpers';
import { BettingOption } from './BettingOption';
import { SelectionProcess } from './SelectionProcess';
import './App.css';

const initialOptions: Record<Option, number> = {
  "Paper": 0,
  "Rock": 0,
  "Scissors": 0
}

enum Screen {
  game,
  result,
  select
}

function App() {
  const [balance, setBalance] = React.useState<number>(START_SUM)
  const [bets, setBets] = React.useState<Record<Option, number>>(initialOptions);
  const [screen, setScreen] = React.useState<Screen>(Screen.game);
  const [resultMessage, setResultMessage] = React.useState<string>();
  const activeBetsCount = getBetsCount(bets)
  const changeBetOn = (option: Option, betChange: number) => {
    if (bets[option] + betChange < 0 || balance - betChange < 0) {
      return
    }
    setBets({
      ...bets,
      [option]: bets[option] + betChange
    });
    setBalance(balance - betChange);
  }
  const doTheGame = () => {
    setScreen(Screen.select);
    setTimeout(() => {
      const aiChoise = chooseRandom(OPTIONS);
      const { payoff, winningPlayerOption, result } = getGameResult(bets, aiChoise);
      setResultMessage(result === 'win' ?
        `Computer chose ${EMOJIS[aiChoise]}. Your ${winningPlayerOption} beats computer's ${aiChoise} and you win ${payoff}`
        : `Computer chose ${EMOJIS[aiChoise]}. You cannot beat ${aiChoise} and you lose your bets`);
      setBalance(balance + payoff);
      setScreen(Screen.result)
    }, 2000)
  }

  return <div className='app'>
    <div className='balance'>{`Balance is ${balance}`}</div>
    <div>{OPTIONS.map(option => `${EMOJIS[option]} ${bets[option]}`).join(' ')}</div>
    {screen === Screen.select && <SelectionProcess />}
    {screen === Screen.game && <>
      <div className={'content'}>
        {OPTIONS.map((option) => (
          <div key={option} className={'bet'}>
            <div>{bets[option]}</div>
            <BettingOption
              disabled={activeBetsCount === MAX_ACTIVE_BETS && bets[option] === 0}
              increaseDisabled={balance === 0}
              decreaseDisabled={bets[option] === 0}
              increaseBet={() => changeBetOn(option, BET_SIZE)}
              decreaseBet={() => changeBetOn(option, -BET_SIZE)}
            >
              {option}
            </BettingOption>
            <div className={'subtitle'}>
              {getSubtitle(bets, option)}
            </div>
          </div>
        ))}
      </div>
      <button
        disabled={activeBetsCount < 1}
        onClick={doTheGame}
        className={'main-action'}
      >
        Confirm Bets
      </button>
    </>}
    {screen === Screen.result && <>
      <div className={'content'}>
        {resultMessage}
      </div>
      <button
        onClick={() => {
          setScreen(Screen.game)
          setBets(initialOptions)
        }}
        className={'main-action'}
      >Play again</button>
    </>
    }
  </div>
}
export default App;