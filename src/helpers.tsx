import { Option, Multiplicator, GameResult } from './types';
import { OPTIONS, OPTIONS_ORDERED_FOR_COMPARE, MAX_ACTIVE_BETS } from './constants';

export function chooseRandom<T>(list: Array<T>): T {
  return list[Math.floor(Math.random() * list.length)];
}

export function compareOptions(a: Option, b: Option): number {
  const aIndex = OPTIONS_ORDERED_FOR_COMPARE.indexOf(a)
  const bIndex = OPTIONS_ORDERED_FOR_COMPARE.indexOf(b)
  if (aIndex === bIndex) {
    return 0;
  }
  if (aIndex === (bIndex + 1) % OPTIONS_ORDERED_FOR_COMPARE.length) {
    return 1
  }
  return -1
}

export const getBetsCount: (bets: Record<Option, number>) => number = bets => OPTIONS.filter(key => bets[key] > 0).length;

export const getMultiplicator: (activeBetsCount: number) => number = (activeBetsCount) => {
  if (activeBetsCount === 1) {
    return Multiplicator.singleOption
  }
  if (activeBetsCount === 2) {
    return Multiplicator.doubleOption
  }
  return 0;
}

export const getSubtitle: (bets: Record<Option, number>, option: Option) => string = (bets, option) => {
  if (bets[option] === 0 && getBetsCount(bets) === MAX_ACTIVE_BETS) {
    return `You cannot bet more than ${MAX_ACTIVE_BETS} options`;
  }
  if (bets[option] > 0) {
    return `You can win ${bets[option] * getMultiplicator(getBetsCount(bets))}`;
  }

  return 'Place your bet to win';
}

export const getGameResult: (playerBets: Record<Option, number>, aiOption: Option) => GameResult = (playerBets, aiOption) => {
  const winningPlayerOption = OPTIONS.filter(option => playerBets[option] > 0 && compareOptions(option, aiOption) > 0)[0];
  const multiplicator = getMultiplicator(getBetsCount(playerBets));

  return {
    result: winningPlayerOption ? 'win' : 'lose',
    payoff: winningPlayerOption ? playerBets[winningPlayerOption] * multiplicator : 0,
    winningPlayerOption
  }
}