import { compareOptions, getGameResult } from './helpers';

test('Paper beats Rock', () => {
  expect(compareOptions("Paper", "Rock")).toBe(1);
  expect(compareOptions("Rock", "Paper")).toBe(-1);
});

test('Rock beats Scissors', () => {
  expect(compareOptions("Rock", "Scissors")).toBe(1);
  expect(compareOptions("Scissors", "Rock")).toBe(-1);
});

test('Scissors beats Paper', () => {
  expect(compareOptions("Scissors", "Paper")).toBe(1);
  expect(compareOptions("Paper", "Scissors")).toBe(-1);
});

test('Nothing beats itself', () => {
  expect(compareOptions("Paper", "Paper")).toBe(0);
  expect(compareOptions("Scissors", "Scissors")).toBe(0);
  expect(compareOptions("Rock", "Rock")).toBe(0);
})

test('Rock and Paper win Scissors', () => {
  const { result, payoff, winningPlayerOption } = getGameResult({ Rock: 1, Paper: 1, Scissors: 0 }, 'Scissors');
  expect(result).toBe('win');
  expect(payoff).toBe(2);
  expect(winningPlayerOption).toBe('Rock')
})

test('Rock wins Scissors', () => {
  const { result, payoff, winningPlayerOption } = getGameResult({ Rock: 1, Paper: 0, Scissors: 0 }, 'Scissors');
  expect(result).toBe('win');
  expect(payoff).toBe(14);
  expect(winningPlayerOption).toBe('Rock')
})

test('Rock and Paper win Rock', () => {
  const { result, payoff, winningPlayerOption } = getGameResult({ Rock: 1, Paper: 1, Scissors: 0 }, 'Rock');
  expect(result).toBe('win');
  expect(payoff).toBe(2);
  expect(winningPlayerOption).toBe('Paper')
})

test('Rock and Paper don\'t win Paper', () => {
  const { result, payoff, winningPlayerOption } = getGameResult({ Rock: 1, Paper: 1, Scissors: 0 }, 'Paper');
  expect(result).toBe('lose');
  expect(payoff).toBe(0);
  expect(winningPlayerOption).toBe(undefined)
})

test('Paper doesn\'t win Paper', () => {
  const { result, payoff, winningPlayerOption } = getGameResult({ Rock: 0, Paper: 1, Scissors: 0 }, 'Paper');
  expect(result).toBe('lose');
  expect(payoff).toBe(0);
  expect(winningPlayerOption).toBe(undefined)
})

test('Rock doesn\'t win Paper', () => {
  const { result, payoff, winningPlayerOption } = getGameResult({ Rock: 1, Paper: 0, Scissors: 0 }, 'Paper');
  expect(result).toBe('lose');
  expect(payoff).toBe(0);
  expect(winningPlayerOption).toBe(undefined)
})